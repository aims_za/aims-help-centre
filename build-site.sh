#!/bin/bash

if [ ! -d static-site ]; then
    mkdir static-site
fi
staticjinja build --outpath=static-site
cp -r static/ static-site
chmod +r static-site/* -R
chmod +r static-site/static/img/screenshots/*

# Remove files that we don't need
rm static-site/{footer,head,about,layout,navigation,herobanner}.html
